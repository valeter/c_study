int binsearch1(int a[], int n, int from, int to) {
	int mid;
	while (from <= to) {
		mid = (from + to) / 2;

		if (n < a[mid])
			to = mid - 1;
		else if (n > a[mid])
			from = mid + 1;
		else 
			return mid;
	}
	return -1;
}

/* best one */
int binsearch2(int a[], int n, int from, int to) {
	int mid;
	while (from < to) {
		mid = (from + to + 1) / 2;
		if (n < a[mid]) to = mid - 1;
		else from = mid;
	}
	return a[from] == n ? from : -1;
}

/* worst one */
int binsearch3(int a[], int n, int from, int to) {
	if (from >= to) return a[from] == n ? from : -1;
	int mid = (from + to + 1) / 2;
	if (n < a[mid]) return binsearch3(a, n, from, mid - 1);
	else return binsearch3(a, n, mid, to);
}


/*
real	0m2.804s
user	0m2.791s
sys		0m0.006s
*/
void test1() {
	int len = 1000000;
	int a[len];
	for (int i = 0; i < len; i++)
		a[i] = i;

	int warmupCycles = 100000;
	int testCycles = 10000000;

	for (int i = 0; i < warmupCycles; i++) {
		binsearch1(a, i, 0, len - 1);
	}
	for (int i = 0; i < testCycles; i++) {
		binsearch1(a, i, 0, len - 1);
	}
}

/*
real	0m2.533s
user	0m2.520s
sys		0m0.007s
*/
void test2() {
	int len = 1000000;
	int a[len];
	for (int i = 0; i < len; i++)
		a[i] = i;

	int warmupCycles = 100000;
	int testCycles = 10000000;

	for (int i = 0; i < warmupCycles; i++) {
		binsearch2(a, i, 0, len - 1);
	}
	for (int i = 0; i < testCycles; i++) {
		binsearch2(a, i, 0, len - 1);
	}
}

/*
real	0m3.119s
user	0m3.075s
sys		0m0.015s
*/
void test3() {
	int len = 1000000;
	int a[len];
	for (int i = 0; i < len; i++)
		a[i] = i;

	int warmupCycles = 100000;
	int testCycles = 10000000;

	for (int i = 0; i < warmupCycles; i++) {
		binsearch3(a, i, 0, len - 1);
	}
	for (int i = 0; i < testCycles; i++) {
		binsearch3(a, i, 0, len - 1);
	}
}

int main() {
	test3();
}