#include <stdio.h>

void printbits(unsigned x) {
	int len = 8 * sizeof(unsigned);
	char b[len];
	printf("%10u = ", x);
	b[len] = '\0';
	while (len > 0) {
		b[--len] = '0' + (x & 1);
		x >>= 1;
	}
	printf("%s\n", b);
}

unsigned getbits(unsigned x, int p, int n) {
	return (x >> (p + 1 - n) & ~(~0 << n)); 
}

unsigned setbits(unsigned x, int p, int n, unsigned y) {
	return (x & (~0 << (p + 1))) | (x & ~(~0 << (p + 1 - n))) | (y & ~(~0 << n));
}

unsigned invert(unsigned x, int p, int n) {
	return (x & (~0 << (p + 1))) | (~x & ~(~0 << n));
}

unsigned rightrot(unsigned x, int n) {
	return (x >> n) | (x << (8 * sizeof(unsigned) - n));
}

int bitcount(unsigned x) {
	int b = 0;
	for(; x != 0; x &= (x - 1))
		b++;
	return b;
}

int main() {
	printf("%u\n", getbits(5u, 1, 2));
	printf("%u\n", setbits(11u, 1, 2, 8u));
	printf("%u\n", setbits(8u, 1, 2, 11u));

	printf("%u\n", invert(8u, 1, 2));
	printf("%u\n", invert(11u, 1, 2));

	printbits(11u);
	printbits(rightrot(11u, 1));
	printf("%d\n", bitcount(11u));
}