#include <stdio.h>

#define TAB_SIZE 4

int main() {
	int c, bpos = 0;
	while ((c = getchar()) != EOF) {
		if (c == ' ') {
			if (++bpos == TAB_SIZE) {
				putchar('\t');
				bpos = 0;
			}
		} else {
			for (int i = 0; i < bpos; i++)
				putchar(' ');
			bpos = 0;

			putchar(c);
		}
	}
	return 0;
}