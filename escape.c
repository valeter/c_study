#include <stdio.h>

void escape(char s[], char d[]) {
	int i, j;
	for (i = j = 0; s[i] != '\0'; i++) 
		switch (s[i]) {
			case '\n':
				d[j++] = '\\';
				d[j++] = 'n';
				break;
			case '\t':
				d[j++] = '\\';
				d[j++] = 't';
				break;
			default:
				d[j++] = s[i];
				break;
		}
	d[j] = '\0';
}

void deescape(char s[], char d[]) {
	int i, j;
	int esc = 0;
	for (i = j = 0; s[i] != '\0'; i++) 
		switch (s[i]) {
			case 't':
				if (esc == 1) {
					d[j++] = '\t';
					esc = 0;
				} else d[j++] = s[i];
				break;
			case 'n':
				if (esc == 1) {
					d[j++] = '\n';
					esc = 0;
				} else d[j++] = s[i];
				break;
			case '\\':
				if (esc == 0) {
					esc = 1; 
					break;
				}
			default:
				if (esc == 1) {
					d[j++] = '\\';
					esc = 0;
				}
				d[j++] = s[i];
				break;
		}
	d[j] = '\0';
}

int main() {
	char s[] = "what a wonderf\\\\ul life\nis\tthere\n";
	char d[1024];
	escape(s, d);
	printf("%s\n", d);
	deescape(d, s);
	printf("%s\n", s);
}