#include <stdio.h>

void expand(char s[], char d[]) {
	int i, j;
	char prev1 = -1, prev2 = -1;
	for (i = j = 0; s[i] != '\0'; i++) {
		if (prev1 == '-' && ((prev2 >= 'a' && prev2 <= 'z') || (prev2 >= 'A' && prev2 <= 'Z') || (prev2 >= '0' && prev2 <= '9'))) {
			if (s[i] >= prev2) {
				for (char c = prev2; c <= s[i]; c++)
					d[j++] = c;
				prev2 = -1;
				prev1 = -1;
				continue;
			} else {
				d[j++] = prev2;
				d[j++] = prev1;
				d[j++] = s[i];
			}
		} else if (s[i + 1] != '-' && (s[i] != '-' || prev1 == -1 || s[i + 1] == '\0')) 
			d[j++] = s[i];

		prev2 = prev1;
		prev1 = s[i];
	}
	d[j] = '\0';
}

int main() {
	char s[] = "-a-b-c0-91-5a-ZA-Zg-i-a-z-";
	char d[1024];
	expand(s, d);
	printf("%s\n", s);
	printf("%s\n", d);
}