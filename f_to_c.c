#include <stdio.h>

#define	LOWER	-100
#define UPPER	200
#define	STEP	20

/*
	program prints fahrenheit to celsius table from -100 F to 200 F with 20 F step
*/
int main() {
	int lower = LOWER;
	int upper = UPPER;
	int step = STEP;
	printf("input lower bound, upper bound and step: ");
	scanf("%d %d %d", &lower, &upper, &step);

	float fahrenheit = lower, celsius;
	while (fahrenheit <= upper) {
		celsius = (5.0 / 9.0) * (fahrenheit - 32.0);
		printf("%4.0f F %6.1f C\n", fahrenheit, celsius);
		fahrenheit += step;
	}
}