#include <stdio.h>
#include <limits.h>
#include <string.h>

void reverse(char s[]) {
	int len = strlen(s);
	int i, tmp;
	for (i = 0; i < len / 2; i++)
		tmp = s[i], s[i] = s[len - i - 1], s[len - i - 1] = tmp;
	s[len] = '\0';
}

void itoa(int n, char s[]) {
	long k = n;
	int i = 0, sign;
	if ((sign = n) < 0)
		k = -k;

	do {
		s[i++] = k % 10 + '0';
	} while ((k /= 10) > 0);
	if (sign < 0)
		s[i++] = '-';
	s[i] = '\0';
	reverse(s);
}

void itoaf(int n, char s[], int w) {
	long k = n;
	int i = 0, sign;
	if ((sign = n) < 0)
		k = -k;

	do {
		s[i++] = k % 10 + '0';
	} while ((k /= 10) > 0);
	if (sign < 0)
		s[i++] = '-';
	for (; i < w; i++)
		s[i] = ' ';
	s[i] = '\0';
	reverse(s);
}

void itob(int n, char s[], int b) {
	long k = n;
	int i = 0, sign;
	if ((sign = n) < 0)
		k = -k;

	do {
		int mod = k % b;
		s[i++] = mod >= 10 ? mod - 10 + 'a' : mod + '0';
	} while ((k /= b) > 0);
	if (sign < 0)
		s[i++] = '-';
	s[i] = '\0';
	reverse(s);
}

int main() {
	char s[1024];
	itoa(-3345345, s);
	printf("%s\n", s);
	itoa(99991, s);
	printf("%s\n", s);
	itoa(INT_MIN, s);
	printf("%s\n", s);
	itoa(INT_MAX, s);
	printf("%s\n", s);

	itob(INT_MIN, s, 16);
	printf("%s\n", s);
	itob(INT_MAX, s, 16);
	printf("%s\n", s);

	itob(INT_MIN, s, 8);
	printf("%s\n", s);
	itob(INT_MAX, s, 8);
	printf("%s\n", s);

	itob(INT_MIN, s, 2);
	printf("%s\n", s);
	itob(INT_MAX, s, 2);
	printf("%s\n", s);


	itoaf(INT_MIN, s, 20);
	printf("%s\n", s);
	itoaf(INT_MAX, s, 20);
	printf("%s\n", s);
}