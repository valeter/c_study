#include <stdio.h>

#define MAXLINE	11
#define PRINT_LONGER_THAN	80


int readline(char line[], int maxline);
void copy(char from[], char to[]);
void print_long_lines();


int main() {
	print_long_lines();
	return 0;
}

void print_long_lines() {
	int len, max;
	char line[MAXLINE], longest[MAXLINE];

	max = 0;
	int cur = 0;
	char start[MAXLINE];
	int lineNum = 0, maxLineNum = 0;
	printf("Lines longer than %d symbols:\n", PRINT_LONGER_THAN);
	while ((len = readline(line, MAXLINE)) > 0) {
		if (cur == 0) {
			lineNum++;
			copy(line, start);
		}
		cur += len;

		if (line[len - 1] == '\n') {
			if (cur > max) {
				max = cur;
				maxLineNum = lineNum;
				copy(start, longest);
			}
			if (cur > PRINT_LONGER_THAN) {
				printf("#%d line with length %d:[%s]\n", lineNum, cur, start);
			}
			cur = 0;
		}
	}
	if (cur > max) {
		max = cur;
		copy(start, longest);
	}
	if (cur > PRINT_LONGER_THAN) {
		printf("#%d line with length %d:[%s]\n", lineNum, cur, start);
	}

	printf("\n");
	printf("Longest line:\n");
	if (max > 0)
		printf("#%d line with length %d:[%s]\n", maxLineNum, max, longest);

	printf("\nLines processed: %d\n", lineNum);
}

int readline(char line[], int maxline) {
	int c, res = 0;
	while (res < maxline - 1 && (c = getchar()) != EOF && c != '\n') {
		line[res] = c;
		res++;
	}
	if (c == '\n') {
		line[res] = c;
		res++;
	}
	line[res] = '\0';
	return res;
}

void copy(char from[], char to[]) {
	for (int i = 0; (to[i] = from[i]) != '\0'; i++)
		;
}