#include <stdio.h>

#define MAXLINE 1001

int max;
char line[MAXLINE];
char longest[MAXLINE];

int readline();
void copy();


int main() {
	int len;

	extern int max;
	extern char longest[];

	max = 0;
	while ((len = readline()) != 0)
		if (len > max) {
			max = len;
			copy();
		}
	if (max > 0)
		printf("%s", longest);
	return 0;
}

int readline() {
	int c, res = 0;

	extern char line[];

	while (res < MAXLINE - 1 && (c = getchar()) != EOF && c != '\n') {
		line[res] = c;
		res++;
	}
	if (c == '\n') {
		line[res] = c;
		res++;
	}
	line[res] = '\0';
	return res;
}

void copy() {
	extern char line[], longest[];

	for (int i = 0; (longest[i] = line[i]) != '\0'; i++)
		;
}