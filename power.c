#include <stdio.h>

int power(int m, int n);

int main() {
	printf(" pow2  pow3  pow4\n");
	for (int i = 0; i < 10; i++) {
		printf("%5d %5d %5d\n", power(i, 2), power(i, 3), power(i, 4));
	}
}


int power(int a, int b) {
	if (b == 0) 
		return 1;
	if (b % 2 == 0)
		return power(a * a, b / 2);
	else 
		return power(a, b - 1) * a;
}