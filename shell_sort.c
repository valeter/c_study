#include <stdio.h>

void shell_sort(int a[], int n) {
	int gap, i, j, tmp;
	for (gap = n / 2; gap > 0; gap /= 2)
		for (i = gap; i < n; i++)
			for (j = i - gap; j >= 0 && a[j] > a[j + gap]; j -= gap) {
				tmp = a[j];
				a[j] = a[j + gap];
				a[j + gap] = tmp;
			}
}

int main() {
	int len = 1000000;
	int a[len];
	for (int i = 0; i < len; i++)
		a[i] = len - i;
	shell_sort(a, len);

	int sorted = 1;
	for (int i = 1; i < len; i++)
		if (a[i - 1] > a[i]) {
			sorted = 0;
			break;
		}

	printf("%d\n", sorted);
}