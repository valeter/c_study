#include <stdio.h>

int abs(int n) {
	if (n < 0) 
		return -n;
	return n;
}

void printBinary(int n) {
	int len = 8 * sizeof(int) + 1;
	char b[len];
	int i;
	for (i = 0; i < len; i++)
		b[i] = '0';
	b[len] = '\0';
	if (n < 0)
		b[0] = '1';
	i = len - 1;
	while (i > 0) {
		b[i--] = '0' + (n & 1);
		n /= 2;
	}

	printf("%s\n", b);
}

int main() {
	printf("2 = %d\t", 2); 
	printBinary(2);
	printf("~2 = %d\t", ~2); 
	printBinary(~2);
	printf("-2 = %d\t", -2); 
	printBinary(-2);
	printf("~-2 = %d\t", ~-2); 
	printBinary(~-2);

	printf("2 | -2 = "); 
	printBinary(2 | -2);
	printf("2 >> 1 = "); 
	printBinary(-2 >> 1);
	printf("2 >> 2 = "); 
	printBinary(-2 >> 2);
	printf("2 >> 10 = "); 
	printBinary(-2 >> 10);
	printf("1 >> 1 = "); 
	printBinary(1 >> 1);
	printf("-1 >> 1 = "); 
	printBinary(-1 >> 1);
	printf("-1 << 32 = "); 
	printBinary(-1 << 32);
	printf("-1 << 31 = "); 
	printBinary(-1 << 31);

	printf("1 << 1 = %d\n", 1 << 1); 
	printf("2 >> 1 = %d\n", 2 >> 1);
	printf("-2 >> 1 = %d\n", -2 >> 1);
	printf("-2 >> 10 = %d\n", -2 >> 10);
	printf("-1 >> 1 = %d\n", -1 >> 1);
}