#include <stdio.h>
#include <float.h>
#include <limits.h>

int main() {
	printf("%20s\tsizeof[%2ld]\tmin[%22d]\tmax[%22d]\tcharbit[%2d]\tmblen[%2d]\n", "char", sizeof(char), CHAR_MIN, CHAR_MAX, CHAR_BIT, MB_LEN_MAX);
	printf("%20s\tsizeof[%2ld]\tmin[%22d]\tmax[%22d]\n", "unsigned char", sizeof(unsigned char), 0, UCHAR_MAX);
	printf("%20s\tsizeof[%2ld]\tmin[%22d]\tmax[%22d]\n", "signed char", sizeof(signed char), SCHAR_MIN, SCHAR_MAX);
	
	printf("%20s\tsizeof[%2ld]\tmin[%22d]\tmax[%22d]\n", "short", sizeof(short), SHRT_MIN, SHRT_MAX);
	printf("%20s\tsizeof[%2ld]\n", "short int", sizeof(short int));
	printf("%20s\tsizeof[%2ld]\tmin[%22d]\tmax[%22d]\n", "unsigned short", sizeof(unsigned short), 0, USHRT_MAX);
	printf("%20s\tsizeof[%2ld]\n", "signed short", sizeof(signed short));

	printf("%20s\tsizeof[%2ld]\tmin[%22d]\tmax[%22d]\n", "int", sizeof(int), INT_MIN, INT_MAX);
	printf("%20s\tsizeof[%2ld]\tmin[%22d]\tmax[%22u]\n", "unsigned int", sizeof(unsigned int), 0, UINT_MAX);
	printf("%20s\tsizeof[%2ld]\n", "signed int", sizeof(signed int));
	
	printf("%20s\tsizeof[%2ld]\tmin[%22ld]\tmax[%22ld]\n", "long", sizeof(long), LONG_MIN, LONG_MAX);
	printf("%20s\tsizeof[%2ld]\n", "long int", sizeof(long int));
	printf("%20s\tsizeof[%2ld]\n", "long long", sizeof(long long));
	printf("%20s\tsizeof[%2ld]\tmin[%22d]\tmax[%22lu]\n", "unsigned long", sizeof(unsigned long), 0 , ULONG_MAX);
	printf("%20s\tsizeof[%2ld]\n", "signed long", sizeof(signed long));

	printf("%20s\tsizeof[%2ld]\tmin[%22E]\tmax[%22E]\n", "float", sizeof(float), FLT_MIN, FLT_MAX);
	printf("%20s\tsizeof[%2ld]\tmin[%22E]\tmax[%22E]\n", "double", sizeof(double), DBL_MIN, DBL_MAX);
	printf("%20s\tsizeof[%2ld]\tmin[%22LE]\tmax[%22LE]\n", "long double", sizeof(long double), LDBL_MIN, LDBL_MAX);

	return 0;
}