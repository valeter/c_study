#include <stdio.h>

#define FALSE 0
#define TRUE 1

void squeeze(char s1[], char s2[]) {
	int i, j;
	for (i = j = 0; s1[i] != '\0'; i++) {
		int found = FALSE;
		for (int k = 0; s2[k] != '\0'; k++)
			if (s2[k] == s1[i]) {
				found = TRUE;
				break;
			}
		
		if (found == FALSE)
			s1[j++] = s1[i];
	}
	s1[j] = '\0';
}

int main() {
	char s1[] = "what a wonderful life!";
	char s2[] = "wow !";
	squeeze(s1, s2);
	printf("%s\n", s1);
}