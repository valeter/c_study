#include <stdio.h>

int main() {
	int c, ns = 0, no = 0;
	int nd[10];
	for (int i = 0; i < 10; i++)
		nd[i] =0;

	while ((c = getchar()) != EOF) {
		if (c >= '0' && c <= '9')
			nd[c - '0']++;
		else if (c == ' ' || c == '\t' || c == '\n')
			ns++;
		else 
			no++;
	}

	printf("digits:");
	for (int i = 0; i < 10; i++)
		printf(" %d", nd[i]);
	printf("; spaces: %d; other: %d;\n", ns, no);
}