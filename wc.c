#include <stdio.h>

#define IN 	1	/* inside a word */
#define OUT 0	/* outside a word */

int main() {
	int c, nc = 0, nl = 0, nw = 0, state = OUT;
	while ((c = getchar()) != EOF) {
		nc++;
		if (c == '\n')
			nl++;
		if (c == ' ' || c == '\t' || c == '\n') {
			state = OUT;
		} else if (state == OUT) {
			state = IN;
			nw++;
		}
	}
	nl++;

	printf("nc %6d\nnl %6d\nnw %6d\n", nc, nl, nw);
}