#include <stdio.h>

#define OUT	0

#define MAX_WORD_LEN	30
#define MAX_BAR_SIZE	10

int main() { 
	int c, wl = OUT;
	int lc[MAX_WORD_LEN];
	for (int i = 0; i < MAX_WORD_LEN; i++) 
		lc[i] = 0;

	while ((c = getchar()) != EOF) {
		if (c == ' ' || c == '\t' || c == '\n') {
			if (wl > 0) 
				lc[wl]++;
			wl = OUT;
		} else {
			wl++;
		}
	}

	float max = 0;
	for (int i = 1; i < MAX_WORD_LEN; i++) 
		if (lc[i] > max) 
			max = lc[i];

	for (int i = 1; i < MAX_WORD_LEN; i++) {
		int barSize = MAX_BAR_SIZE * (lc[i] / max);
		if (barSize == 0)
			continue;
		printf("%3d |", i);
		for (int j = 0; j < barSize; j++) 
			printf("=");
		printf("|\n");
	}
}